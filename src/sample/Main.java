package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Product;
import model.User;

import java.util.ArrayList;

public class Main extends Application {

    private static Main main;

    private static ArrayList<User> userList;
    private static ArrayList<Product> productList;
    private static Stage mainStage;
    private static User currentUser;

    public static synchronized Main getPrincipal() {
        if(main == null)
            main = new Main();

        return main;
    }

    public static Stage getMainStage() {
        return mainStage;
    }

    public static ArrayList<User> getUserList() {
        return userList;
    }

    public static void addToUserList(User user) {
        Main.userList.add(user);
    }

    public static ArrayList<Product> getProductList() {
        return productList;
    }

    public static void addToProductList(Product product) {
        Main.productList.add(product);
    }

    public static void removeFromProductList(Product product) {
        Main.productList.remove(product);
    }

    public static User getCurrentUser() {
        return Main.currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        Main.currentUser = currentUser;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        main = this;
        primaryStage.setTitle("Distributed Store");
        mainStage = primaryStage;

        try {
            Parent root = FXMLLoader.load(getClass().getResource("../view/login.fxml"));

            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();


        } catch(Exception e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {

        userList = new ArrayList<User>();
        productList = new ArrayList<Product>();

        userList.add(new User("dinalopes", "Diná Lopes", 123456, 'E'));
        userList.add(new User("mackson", "Mackson Silva", 123456, 'C'));
        userList.add(new User("alfredo", "Alfredo Silva", 123456, 'C'));

        productList.add(new Product(100001, "iPhone X", 150f, "T", 20));
        productList.add(new Product(100002, "Moto G", 400f, "T", 15));
        productList.add(new Product(100003, "Lg K8", 550f, "T", 25));
        productList.add(new Product(100004, "Samsung S9", 200f, "T", 10));
        productList.add(new Product(200001, "Cheese Jucurutu", 12f, "F", 100));
        productList.add(new Product(200002, "Milk Betânia", 6f, "F", 140));
        productList.add(new Product(200003, "Ice Cream Frosty", 13f, "F", 90));
        productList.add(new Product(200004, "Eggs Forte Gema", 12f, "F", 16));
        productList.add(new Product(300001, "Bed Ortobom", 600f, "M", 30));
        productList.add(new Product(300002, "Chair Plasutil", 22f, "M", 40));
        productList.add(new Product(300003, "TV Philco", 420f, "M", 15));
        productList.add(new Product(300004, "Cabinetry Itaitaia", 1200f, "M", 10));

        System.out.println(userList);
        System.out.println(productList);

        launch(args);
    }
}
