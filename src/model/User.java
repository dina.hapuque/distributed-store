package model;

public class User {

    private String userID;
    private String name;
    private int password;
    private char roleID;

    public User() {};

    public User(String userID, String name, int password, char roleID) {
        setUserID(userID);
        setName(name);
        setPassword(password);
        setRoleID(roleID);
    }

    public void setUserID(String userID) {
        if(!userID.isEmpty()) this.userID = userID;
        else this.userID = "Invalid userID";
    }

    public String getUserID() {
        return this.userID;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if(!name.isEmpty()) this.name = name;
        else this.name = "Invalid name";
    }

    public int getPassword() {
        return this.password;
    }

    public void setPassword(int password) {
        if(password > 999 && password < 10000) this.password = password;
        else this.password = 1234;
    }

    public char getRoleID() {
        return this.roleID;
    }

    public void setRoleID(char roleID) {
        if (roleID == 'C' || roleID == 'E') this.roleID = roleID;
        else this.roleID = 'C';
    }

}
