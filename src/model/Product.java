package model;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Product {

    private SimpleIntegerProperty code;
    private SimpleStringProperty description;
    private SimpleFloatProperty price;
    private SimpleStringProperty category;
    private SimpleIntegerProperty storage;

    public Product() {};

    public Product(int code, String description, float price, String category, int storage) {
        setCode(code);
        setDescription(description);
        setPrice(price);
        setCategory(category);
        setStorage(storage);
    }


    public int getCode() {
        return code.get();
    }

    public void setCode(int code) {
        if(code > 99999 && code < 1000000)
            this.code = new SimpleIntegerProperty(code);
        else
            this.code = new SimpleIntegerProperty(0);
    }

    public SimpleIntegerProperty codeProperty() {
        return this.code;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        if (!description.isEmpty())
            this.description = new SimpleStringProperty(description);
        else
            this.description = new SimpleStringProperty("Invalid description");
    }

    public SimpleStringProperty descriptionProperty() {
        return this.description;
    }

    public float getPrice() {
        return price.get();
    }

    public void setPrice(float price) {
        if (price > 0 && price < 100000)
            this.price = new SimpleFloatProperty(price);
        else
            this.price = new SimpleFloatProperty(0);
    }

    public SimpleFloatProperty priceProperty() {
        return this.price;
    }

    public String getCategory() {
        return this.category.get();
    }

    public void setCategory(String category) {
        if (category.equals("F") || category.equals("T") || category.equals("M"))
            this.category = new SimpleStringProperty(category);
        else
            this.category = new SimpleStringProperty("I");
    }

    public SimpleStringProperty categoryProperty() {
        return this.category;
    }

    public int getStorage() {
        return this.storage.get();
    }

    public void setStorage(int storage) {
        if (storage > 0 && storage < 100000)
            this.storage = new SimpleIntegerProperty(storage);
        else
            this.storage = new SimpleIntegerProperty(0);
    }

    public SimpleIntegerProperty storageProperty() {
        return this.storage;
    }

}
