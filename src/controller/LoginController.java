package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import sample.Main;

import java.io.IOException;


public class LoginController {

    @FXML private TextField userIDTextField;
    @FXML private TextField passwordTextField;

    @FXML
    private void initialize() {

    }

    @FXML private void onLoginPress() {
        String userID = userIDTextField.getText();
        int password = Integer.parseInt(passwordTextField.getText());

        for (int i = 0; i < Main.getUserList().size(); i++ ) {
            if (userID.equals(Main.getUserList().get(i).getUserID())) {

                if (password == Main.getUserList().get(i).getPassword()) {
                    FXMLLoader loader = new FXMLLoader(this.getClass().getResource("../view/principal.fxml"));
                    try {
                        Main.setCurrentUser(Main.getUserList().get(i)); // setting current user
                        Main.getMainStage().setScene(new Scene(loader.load()));
                        Main.getMainStage().setTitle("Home");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (Main.getCurrentUser() == null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Something's wrong");
            alert.setContentText("Nothing found with this credentials");

            alert.showAndWait();
        }

    }



}
