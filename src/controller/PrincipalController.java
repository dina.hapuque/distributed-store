package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import model.Product;
import sample.Main;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class PrincipalController implements Initializable {

    @FXML private Pane employeePane; // disabled if roleID == 'C'
    @FXML private TableView productTableView;
    @FXML private TableColumn<Product, Integer> codeTableColumn;
    @FXML private TableColumn<Product, String> descriptionTableColumn;
    @FXML private TableColumn<Product, String> categoryTableColumn;
    @FXML private TableColumn<Product, Float> priceTableColumn;
    @FXML private TableColumn<Product, Integer> storageTableColumn;
    @FXML private TextField categoryTextField;
    @FXML private TextField codeTextField;
    @FXML private TextField descriptionTextField;
    @FXML private TextField priceTextField;
    @FXML private TextField storageTextField;
    @FXML private TextField qtyTextField;
    @FXML private TextField searchTextField;
    @FXML private Button clearSelectionButton;
    @FXML private Button deleteButton;
    @FXML private Button addOrUpdateButton;
    @FXML private Label informationsLabel;
    @FXML private Label searchLabel;

    private Product selectedProduct;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        if (Main.getCurrentUser().getRoleID() == 'C') {

            employeePane.setVisible(false);

        }

        codeTableColumn.setCellValueFactory(new PropertyValueFactory<>("code"));
        descriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        categoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
        priceTableColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        storageTableColumn.setCellValueFactory(new PropertyValueFactory<>("storage"));

        onRefreshPress();

        clearSelectionButton.setVisible(false);
        deleteButton.setVisible(false);
        addOrUpdateButton.setText("Add");

        productTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {

            if (newSelection == null) {
                clearSelectionButton.setVisible(false);
                deleteButton.setVisible(false);
                addOrUpdateButton.setText("Add");

            } else {

                clearSelectionButton.setVisible(true);
                deleteButton.setVisible(true);
                addOrUpdateButton.setText("Update");

                selectedProduct = (Product) newSelection;

                codeTextField.setText(String.valueOf(selectedProduct.getCode()));
                descriptionTextField.setText(selectedProduct.getDescription());
                categoryTextField.setText(selectedProduct.getCategory());
                priceTextField.setText(String.valueOf(selectedProduct.getPrice()));
                storageTextField.setText(String.valueOf(selectedProduct.getStorage()));
            }

        });
    }



    @FXML
    private void onSearchPress() {
        String text = searchTextField.getText();

        ArrayList<Product> search = new ArrayList<Product>();


        if (text.isEmpty()) {
            onRefreshPress(); // all items
            searchLabel.setText("All products");
        } else {

            for(Product p: Main.getProductList()){
                String code = String.valueOf(p.getCode());

                if (p.getDescription().contains(text) || code.contains(text))
                    search.add(p);
            }

            onRefreshPress(search);
            searchLabel.setText(search.size() + " product(s) found");
        }

    }

    @FXML
    private void onAddOrUpdatePress() {

        if (selectedProduct == null) {
            Product newProduct = new Product();
            Main.addToProductList(newProduct);
            selectedProduct = newProduct;
        }

        selectedProduct.setCode(Integer.parseInt(codeTextField.getText()));
        selectedProduct.setDescription(descriptionTextField.getText());
        selectedProduct.setCategory(categoryTextField.getText());
        selectedProduct.setPrice(Float.valueOf(priceTextField.getText()));
        selectedProduct.setStorage(Integer.parseInt(storageTextField.getText()));

        onRefreshPress();
        clearFields();
    }

    @FXML
    private void onDeletePress() {
        Main.removeFromProductList(selectedProduct);
        onRefreshPress();
        clearFields();

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText("Be aware");
        alert.setContentText(selectedProduct.getDescription() + " was deleted!");

        alert.showAndWait();
    }

    @FXML
    private void onBuyPress() {

        int qty = Integer.parseInt(qtyTextField.getText());
        int actualQty = selectedProduct.getStorage();
        if (actualQty - qty >= 0) {
            selectedProduct.setStorage(actualQty - qty);

            informationsLabel.setText("That cost " + selectedProduct.getPrice() * qty + " floats");

            onRefreshPress();
            clearFields();


        } else {
            informationsLabel.setText("Not enough storage");
        }

    }

    @FXML
    private void onRefreshPress() {
        productTableView.setItems(FXCollections.observableArrayList(Main.getProductList()));
        productTableView.refresh();
    }

    private void onRefreshPress(ArrayList<Product> arrayList) {
        productTableView.setItems(FXCollections.observableArrayList(arrayList));
        productTableView.refresh();
    }

    @FXML
    private void onClearSelectionPress() {
        selectedProduct = null;

        productTableView.getSelectionModel().clearSelection();

        codeTextField.clear();
        descriptionTextField.clear();
        categoryTextField.clear();
        priceTextField.clear();
        storageTextField.clear();

    }

    private void clearFields() {
        codeTextField.clear();
        descriptionTextField.clear();
        categoryTextField.clear();
        priceTextField.clear();
        storageTextField.clear();
        qtyTextField.clear();
        searchTextField.clear();
    }

}
